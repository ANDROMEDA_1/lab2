
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/blkdev.h>
#include <linux/types.h>

#include <linux/proc_fs.h>  
#include <linux/pid.h>  
#include <linux/device.h> 
#include <linux/mutex.h>
#include "unistd.h"
 
#define BUF_SIZE 1024

MODULE_LICENSE("Dual BSD/GPL"); 
MODULE_DESCRIPTION("lab2"); 
MODULE_VERSION("1.0");

static int pid = 1; 
static struct mutex mtx;

 
static struct proc_dir_entry *parent;

static int      __init lab_driver_init(void); 
static void     __exit lab_driver_exit(void);

/***************** Procfs Functions *******************/ 
static int      open_proc(struct inode *inode, struct file *file); 
static int      release_proc(struct inode *inode, struct file *file); 
static ssize_t  read_proc(struct file *filp, char __user *buffer, size_t 
length,loff_t * offset); 
static ssize_t  write_proc(struct file *filp, const char *buff, size_t len, 
loff_t * off); 
 
/* 
** procfs operation sturcture 
*/ 
static struct file_operations proc_fops = { 
        .open = open_proc,  
        .read = read_proc, 
        .write = write_proc, 
        .release = release_proc 
};

static size_t write_task_cputime(char __user *buffer,
				struct task_struct *ts) {
	struct task_cputime tct;
	
	

	tct.stime = ts->stime;
	tct.utime = ts->utime;
	tct.sum_exec_runtime = tct.stime + tct.utime;


	size_t len = 0;
	char buf[BUF_SIZE];

	len += sprintf(buf + len, "task_cputime:\n");
	len += sprintf(buf + len, "	stime = %llu\n", tct.stime);
	len += sprintf(buf + len, "	utime = %llu\n", tct.utime);
	len += sprintf(buf + len, "	sum_exec_runtime = %llu\n", tct.sum_exec_runtime);

	if (copy_to_user(buffer, buf, len)) {
		return -EFAULT;
	}

	return len;
}



static size_t write_inode(char __user *buffer,
				struct task_struct *ts) {
	size_t len = 0;
	char buf[BUF_SIZE];
	struct mm_struct *mm = ts->mm;
	len += sprintf(buf + len, "inode:\n");
	if (mm == NULL){
		len += sprintf(buf + len, "	ts->mm is NULL");
		if (copy_to_user(buffer, buf, len)) {
			return -EFAULT;
		}
		return len;
	}
	printk(KERN_INFO "mm finded");

	struct vm_area_struct *vm = mm->mmap;
	if (vm == NULL){
		len += sprintf(buf + len, "	ts->mm->mmap is NULL");
		if (copy_to_user(buffer, buf, len)) {
			return -EFAULT;
		}
		return len;
	}

	struct file *fl = vm->vm_file;
	if (fl == NULL){
		len += sprintf(buf + len, "	ts->mm->mmap->vm_file is NULL");
		if (copy_to_user(buffer, buf, len)) {
			return -EFAULT;
		}
		return len;
	}
	
	struct inode *inode = fl->f_inode;
	if (inode == NULL){
		len += sprintf(buf + len, "	ts->mm->mmap->vm_file->f_inode is NULL");
		if (copy_to_user(buffer, buf, len)) {
			return -EFAULT;
		}
		return len;
	}

	len += sprintf(buf + len, "	i_ino: %ld\n", inode->i_ino);
	len += sprintf(buf + len, "	i_mode: %d\n", inode->i_mode);
	len += sprintf(buf + len, "	i_nlink: %d\n", inode->i_nlink);
	len += sprintf(buf + len, "	i_rdev: %d\n", inode->i_rdev);
	len += sprintf(buf + len, "	i_size: %lld\n", inode->i_size);
	len += sprintf(buf + len, "	i_blocks: %lld\n", inode->i_blocks);

	if (copy_to_user(buffer, buf, len)) {
		return -EFAULT;
	}

	return len;
}	


// otkritie
static int open_proc(struct inode *inode, struct file *file) 
{ 
	mutex_lock(&mtx);
    printk(KERN_INFO "proc file opend.....\t"); 
    return 0; 
} 
 
//zakritie
static int release_proc(struct inode *inode, struct file *file) 
{ 
	mutex_unlock(&mtx);
    	printk(KERN_INFO "proc file released.....\n"); 
    	return 0; 
}

//chetie

static ssize_t read_proc(struct file *filp, 
			char __user *ubuf, 
			size_t count, 
			loff_t *ppos) { 
    	char buf[BUF_SIZE]; 
    	int len = 0; 
    		struct task_struct *task_struct_ref = get_pid_task(find_get_pid(pid), PIDTYPE_PID); 
     
    	printk(KERN_INFO "proc file readaaaaaaa.....\n"); 
    	if (*ppos > 0 || count < BUF_SIZE){ 
        	return 0; 
    	} 
 
	
    if (task_struct_ref == NULL){ 
	
        len += sprintf(buf,"task_struct for pid %d is NULL.\n",pid); 
 
        if (copy_to_user(ubuf, buf, len)){ 
	
            return -EFAULT; 
        } 

        *ppos = len; 
        return len; 
    } 
    	len += write_task_cputime(ubuf + len, task_struct_ref);
	len += write_inode(ubuf + len, task_struct_ref);
 
    	*ppos = len; 
    	return len; 
}


// zapis'
static ssize_t write_proc(struct file *filp, const char __user *ubuf, size_t 
count, loff_t *ppos) { 

    	int num_of_args, c, b, adr; 
    	char buf[BUF_SIZE];
     
    	printk(KERN_INFO "proc file wrote.....\n"); 
 
    	if (*ppos > 0 || count > BUF_SIZE){ 
        	return -EFAULT; 
    	} 
 
    	if( copy_from_user(buf, ubuf, count) ) { 
        	return -EFAULT; 
    	} 
 
    	num_of_args = sscanf(buf, "%d",  &b); 
    	if (num_of_args != 1){ 
        	return -EFAULT; 
    	} 

    

    
    	pid = b;
 
    	c = strlen(buf); 
    	*ppos = c; 
    	return c; 
}

// init
static int __init lab_driver_init(void) { 
	mutex_init(&mtx);
    	parent = proc_mkdir("lab2",NULL); 
 
    	if( parent == NULL ) 
    	{ 
        	pr_info("Error creating proc entry"); 
        	return -1; 
    	} 
 
    	proc_create("struct_info", 0666, parent, &proc_fops); 
 
    	pr_info("Device Driver Insert...Done!!!\n"); 
    	return 0; 
} 
 
// exit from module
static void __exit lab_driver_exit(void) 
{ 
    // dlya 1 zapisi
    //remove_proc_entry("lab/struct_info", parent); 
 
    // dlya full udaleniya
    proc_remove(parent); 
     
    pr_info("Device Driver Remove\nDone.\n"); 
} 
 
module_init(lab_driver_init); 
module_exit(lab_driver_exit);




